let flipOptions = {
  uek1: true,
  uek2: true,
  uek3: true,
  uek4: true
};

function expandBox(event, uek) {
  flipOptions[uek] = !flipOptions[uek]; //toggle if flipped or not

  flipIcon(event, uek); //flip chevron function

  const targetBox = event.path[1];

  flipOptions[uek]
    ? targetBox.classList.remove("expand")
    : targetBox.classList.add("expand");
}

function flipIcon(event, uek) {
  const targetHeader = event.toElement; //get clicked box
  const icon = targetHeader.children[1];

  flipOptions[uek]
    ? icon.classList.remove("fa-flip-vertical")
    : icon.classList.add("fa-flip-vertical");
}
